<?php

use App\Http\Controllers\Api\V1\AuthController;
use App\Http\Controllers\Api\V1\UserController;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['middleware' => ['corsguest'], 'prefix' => 'v1'], function () {
    // create user
    Route::post('/register', [AuthController::class, 'register']);
    // login  user
    Route::post('/login', [AuthController::class, 'login'])->name('login');
    // logout  user
    Route::get('/logout', [AuthController::class, 'logout'])->name('logout');
    Route::group(['prefix' => 'user'], function () {
        Route::get('/fake-it', [UserController::class, 'fakeUser']);

        Route::group(['middleware' => 'auth:passport'], function () {
            Route::get('/getUser', [UserController::class, 'getAuthenticatedUser']);
        });
    });
});

