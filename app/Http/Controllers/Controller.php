<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    /**
     * @OA\Info(
     *      version="1.0.0",
     *      title="TurkApp",
     *      description="TurkApp Laravel Api",
     *      @OA\Contact(
     *          email="ehsan.jnra@gmail.com"
     *      ),
     * )
     *
     * @OA\Server(
     *      url=L5_SWAGGER_CONST_HOST,
     *      description="Development API"
     * )

     *
     * @OA\Tag(
     *     name="User",
     *     description="API Endpoints of User"
     * )
     */

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

}
