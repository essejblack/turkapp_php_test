<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\User;
use Database\Factories\UserFactory;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;
use Illuminate\Support\Facades\Validator;

/**
 *
 * @OA\SecurityScheme(
 *      securityScheme="bearerAuth",
 *      type="http",
 *      scheme="bearer"
 * )
 */
class UserController extends AuthController
{
    /**
     * @OA\Get(
     * path="/user/getUser",
     * operationId="getuser",
     * tags={"User"},
     * summary="Get User Detail",
     * description="Get User Detail",
     * security={{"bearerAuth":{}}},
     *      @OA\Response(
     *          response=201,
     *          description="Success",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=422,
     *          description="Unprocessable Entity",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Token invalid"),
     * )
     */
    public function getAuthenticatedUser(Request $request): string
    {
        $user = $request->user();
        return response()->json(compact('user'));
    }


    public function fakeUser()
    {
        $fakeuser = User::factory()->count(1)->make()[0];
        $user = User::create([
            'name' => $fakeuser->name,
            'email' => $fakeuser->email,
            'password' => Hash::make($fakeuser->password),
        ]);
        if ($user) {
            return response()->json(['massage' => 'user_created.', 'data' => $user]);
        } else {
            return response()->json(['error' => 'could_not_create_user', 'data' => $user], 500);
        }
    }

}
