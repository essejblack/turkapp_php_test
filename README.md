<p align="center"><a href="https://turkapp.com/" target="_blank"><img style="max-width: 150px;border-radius:50%" src="https://thumb.jobinjacdn.com/7B3ONMJmQz_CkbI-3i2HmTWDIhU=/fit-in/128x128/filters:strip_exif():fill(white):quality(100)/https://storage.jobinjacdn.com/other/files/uploads/images/cccf4e22-4068-489b-92a9-02f2ef648c39/main.jpg" width="400"></a></p>

## TurkApp Test

first off all you need to read laravel Doc:

- [Laravel Doc](https://laravel.com/docs/8.x).

Make your first laravel project .

``
composer create-project laravel/laravel
``
we will use sqlite as DBMS

Passport :

````
composer require laravel/passport
php artisan migrate
php artisan passport:install
composer require "darkaonline/l5-swagger"
php artisan vendor:publish --provider "L5Swagger\L5SwaggerServiceProvider"
php artisan l5-swagger:generate
````

- Open the Documentation

App_URL + /api/documentation
